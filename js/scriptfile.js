//#region Get nodes
let inputField = document.getElementById('input-field');
let addButton = document.getElementById('add-button');
let initialTemplate = document.getElementsByClassName("template")[0].cloneNode(true);
let objectsDiv = document.getElementById("objects");
let indexInput = document.getElementById("index-input");
//#endregion

//#region Add element input
addButton.addEventListener("click", e => {
    addElement();
});

inputField.onkeypress = function(e) {
    if (!e) e = window.event;
    var keyCode = e.keyCode || e.which;
    if (keyCode == '13') addElement();
}
//#endregion


function addElement() {
    let indexInputValue = clamp(parseInt(indexInput.value), 0, objectsDiv.children.length);
    let clone = initialTemplate.cloneNode(true);
    let nameElement = clone.querySelector(".name");

    if(inputField.value == "") inputField.value = "default";

    nameElement.innerHTML = inputField.value;

    //#region Mouse events
    clone.addEventListener("click", (e) => {
        clone.remove();
    });

    // o botao direito eh usado para edicao
    clone.addEventListener("contextmenu", e => {
        if(inputField.value == "") inputField.value = "default";
        nameElement.innerHTML = inputField.value;
    });
    //#endregion

    switch (objectsDiv.children.length) {
        case 0:
            objectsDiv.appendChild(clone);
            break;
        default:
            if (objectsDiv.children.length == indexInputValue)
                objectsDiv.children[objectsDiv.children.length - 1].after(clone);
            else
                objectsDiv.children[indexInputValue].before(clone);
            break;
    }

    inputField.value = "";
}

function clamp(value, min, max) {
    if (value >= max) return max;
    if (value <= min) return min;
    return value;
}